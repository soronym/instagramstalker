/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.pictures;

import static org.apache.http.client.fluent.Request.Get;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.UriBuilder;

import org.apache.http.client.fluent.Content;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hlinka.model.StalkLink;
import com.hlinka.model.dao.UserDAO;
import com.hlinka.services.FilterService;

/**
 * {@inheritDoc}.
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Component
public class InstaRequesterImpl
    implements InstaRequester
{

    private static final String INSTA_URL = "https://www.instagram.com";
    private static final String LINE_BEGINNING = "<script type=\"text/javascript\">window._sharedData =";
    private static final String LINE_ENDING = ";</script>";

    private final FilterService filterService;
    private final UserDAO userDAO;

    @Autowired
    public InstaRequesterImpl(final FilterService filterService, final UserDAO userDAO) {
        this.filterService = filterService;
        this.userDAO = userDAO;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public List<StalkLink> getLinksByUsername(String username, Long userId) {

        List<StalkLink> linksByUsername = new ArrayList<>();
        String content = null;

        try {
            content = getContentOfOverview(username).asString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (content != null && !content.isEmpty()) {
            linksByUsername = getLinksFromContent(content, username);
        }
        List<StalkLink> retVal = filterService.filterBasedOnTime(linksByUsername, userDAO.findById(userId).get());
        return retVal;
    }

    private List<StalkLink> getLinksFromContent(String content, String username) {
        JSONObject jsonObject = null;

        String[] contentByLines = content.split("\n");
        Optional<String> lineWithLinks = Arrays.stream(contentByLines).filter(line -> line.contains(LINE_BEGINNING)).findFirst();
        if (lineWithLinks.isPresent()) {
            String line = lineWithLinks.get();
            line = line.replace(LINE_BEGINNING, "");
            line = line.replace(LINE_ENDING, "");
            try {
                jsonObject = new JSONObject(line);
            } catch (JSONException e) {
                throw new RuntimeException("Failed to convert line with links to json");
            }
        }
        return getLinksFromJson(jsonObject, username);
    }

    /**
     * Due to time reasons i'm not mapping JSON onto java object which would be cleaner that this brute explicit casting.
     *
     * @param jsonObject
     * @return
     */
    private List<StalkLink> getLinksFromJson(JSONObject jsonObject, String username) {
        List<StalkLink> stalkLinkList = new ArrayList<>();
        try {
            JSONObject entry_data = (JSONObject) jsonObject.get("entry_data");
            JSONArray profilePage = (JSONArray) entry_data.get("ProfilePage");
            JSONObject zeroElement = (JSONObject) profilePage.get(0);
            JSONObject user = (JSONObject) zeroElement.get("user");
            JSONObject media = (JSONObject) user.get("media");
            JSONArray nodes = (JSONArray) media.get("nodes");
            for (int i = 0; i < nodes.length(); i++) {
                JSONObject node = (JSONObject) nodes.get(i);
                JSONObject dimensions = (JSONObject) node.get("dimensions");
                // Instagram time = time from API * 1000
                LocalDate dateOfPicture = Instant.ofEpochMilli(Long.valueOf(node.get("date").toString() + "000")).atZone(ZoneId.systemDefault()).toLocalDate();

                stalkLinkList.add(new StalkLink(node.get("display_src").toString(), Integer.valueOf(dimensions.get("width").toString()), Integer.valueOf(dimensions.get("height").toString()),
                                                node.get("caption").toString(), dateOfPicture));
            }
        } catch (JSONException e) {
            throw new RuntimeException("Failed to get pictures for a specific account");
        }
        return stalkLinkList;
    }

    private Content getContentOfOverview(String username)
        throws IOException
    {
        final URI uri = UriBuilder.fromUri(INSTA_URL).path(username).build();

        return Get(uri).execute().returnContent();
    }

}
