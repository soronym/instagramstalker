/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hlinka.model.StalkLink;
import com.hlinka.model.User;
import com.hlinka.model.dao.UserDAO;
import com.hlinka.pictures.InstaRequester;

/**
 * Controller for main page.
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Controller
public class MainPageController {

    private final InstaRequester instaRequester;
    private final UserDAO userDAO;

    @Autowired
    public MainPageController(final InstaRequester instaRequester, final UserDAO userDAO) {
        this.instaRequester = instaRequester;
        this.userDAO = userDAO;
    }

    /**
     * Jsp page for main page.
     *
     * @return jsp to show
     */
    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public String showMainPAge(Model model, HttpSession httpSession) {
        String location = "unauthorized";
        if (httpSession.getAttribute("currentUser") != null) {
            User user = (User) httpSession.getAttribute("currentUser");
            user = userDAO.findById(user.getId()).get();
            final Long userId = user.getId();
            List<StalkLink> listOfLinks = new ArrayList<>();

            if (user.getStalkAccountList() != null) {
                user.getStalkAccountList().forEach(stalkAccount -> listOfLinks.addAll(instaRequester.getLinksByUsername(stalkAccount.getTitle(), userId)));
            }
            List<StalkLink> links = listOfLinks.stream().filter(stalkLink -> stalkLink.isActive()).collect(Collectors.toList());
            List<StalkLink> oldLinks = listOfLinks.stream().filter(stalkLink -> !stalkLink.isActive()).collect(Collectors.toList());
            if (!links.isEmpty()) {
                model.addAttribute("newLinksBool", true);
                model.addAttribute("links", listOfLinks.stream().filter(stalkLink -> stalkLink.isActive()).collect(Collectors.toList()));
            }
            if (!oldLinks.isEmpty()) {
                model.addAttribute("oldLinksBool", true);
                model.addAttribute("oldLinks", listOfLinks.stream().filter(stalkLink -> !stalkLink.isActive()).collect(Collectors.toList()));
            }
            location = "mainPage";

        }

        return location;
    }

}
