/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hlinka.model.TimeFilter;
import com.hlinka.model.User;
import com.hlinka.model.dao.StalkAccountDAO;
import com.hlinka.model.dao.StalkGroupDAO;
import com.hlinka.model.dao.UserDAO;

/**
 * Controller to handle settings for an account.
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Controller
@RequestMapping(value = "/settings")
public class SettingsController {

    private final StalkAccountDAO stalkAccountDAO;

    private final StalkGroupDAO stalkGroupDAO;

    private final UserDAO userDAO;

    @Autowired
    public SettingsController(final StalkAccountDAO stalkAccountDAO, final StalkGroupDAO stalkGroupDAO, final UserDAO userDAO) {
        this.stalkAccountDAO = stalkAccountDAO;
        this.stalkGroupDAO = stalkGroupDAO;
        this.userDAO = userDAO;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showSettings(HttpSession httpSession, Model model, @RequestParam(required = false) String show) {
        String location = "unauthorized";
        if (httpSession.getAttribute("currentUser") != null) {
            User user = (User) httpSession.getAttribute("currentUser");
            user = userDAO.findById(user.getId()).get();
            model.addAttribute("stalkAccountList", stalkAccountDAO.findMultipleByUsernameWithoutDuplicities(user.getUsername()));
            model.addAttribute("stalkGroupList", stalkGroupDAO.findMultipleByUsername(user.getUsername()));
            model.addAttribute("timeFiltersUser", filterUniqueFilters(user.getTimeFilterList()));
            model.addAttribute("user", user);
            showSettingGroup(show, model);
            location = "settings";
        }

        return location;
    }

    private List<TimeFilter> filterUniqueFilters(List<TimeFilter> timeFilterList) {
        Set<Long> longSet = timeFilterList.stream().map(timeFilter -> timeFilter.getValueInDays()).collect(Collectors.toSet());

        //        List<TimeFilter> retVal = longSet.stream().map(aLong -> new TimeFilter(aLong)).collect(Collectors.toList());
        List<TimeFilter> retVal = new ArrayList<>();
        for (Long id : longSet) {
            Optional<TimeFilter> filter = timeFilterList.stream().filter(timeFilter -> timeFilter.getValueInDays().equals(id)).findFirst();
            if (filter.isPresent()) {
                retVal.add(filter.get());
            }

        }
        return retVal;
    }

    private void showSettingGroup(String show, Model model) {
        if (show != null) {
            if (show.equalsIgnoreCase("groups")) {
                model.addAttribute("accounts", false);
                model.addAttribute("filters", false);
                model.addAttribute("groups", true);
                model.addAttribute("notifications", false);
            } else if (show.equalsIgnoreCase("filters")) {
                model.addAttribute("accounts", false);
                model.addAttribute("filters", true);
                model.addAttribute("groups", false);
                model.addAttribute("notifications", false);
            } else if (show.equalsIgnoreCase("notifications")) {
                model.addAttribute("accounts", false);
                model.addAttribute("filters", false);
                model.addAttribute("groups", false);
                model.addAttribute("notifications", true);
            } else {
                model.addAttribute("accounts", true);
                model.addAttribute("filters", false);
                model.addAttribute("groups", false);
                model.addAttribute("notifications", false);
            }
        }
    }

}
