/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.controllers;

import java.util.Optional;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hlinka.model.StalkGroup;
import com.hlinka.model.User;
import com.hlinka.model.dao.FiltersDAO;
import com.hlinka.model.dao.StalkGroupDAO;
import com.hlinka.model.dao.UserDAO;

/**
 * {description}
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Controller
@RequestMapping(value = "/timeFilter")
public class TimeFilterController {

    private final FiltersDAO timeFilter;
    private final UserDAO userDAO;
    private final StalkGroupDAO stalkGroupDAO;

    @Autowired
    public TimeFilterController(final FiltersDAO timeFilter, final UserDAO userDAO, final StalkGroupDAO stalkGroupDAO) {
        this.timeFilter = timeFilter;
        this.userDAO = userDAO;
        this.stalkGroupDAO = stalkGroupDAO;
    }

    @RequestMapping(value = "/remove/group", method = RequestMethod.GET)
    public String removeFilter(@RequestParam(value = "groupId") final String groupId, @RequestParam(value = "filterId") final String filterId, HttpServletRequest httpRequest)
    {
        if (httpRequest.getSession().getAttribute("currentUser") != null) {
            timeFilter.deleteForGroup(Long.valueOf(groupId), Long.valueOf(filterId));
        }
        return "redirect:/settings?show=filters";
    }

    @RequestMapping(value = "/remove/account", method = RequestMethod.GET)
    public String removeFilterAccount(@RequestParam(value = "filterId") final String filterId, HttpServletRequest httpRequest)
    {
        if (httpRequest.getSession().getAttribute("currentUser") != null) {
            User user = (User) httpRequest.getSession().getAttribute("currentUser");
            user = userDAO.findByUsername(user.getUsername()).get();
            timeFilter.deleteForAccount(user.getId(), Long.valueOf(filterId));
        }
        return "redirect:/settings?show=filters";
    }

    @RequestMapping(value = "/add/account", method = RequestMethod.POST)
    public String addTimeFilterAccount(HttpServletRequest httpRequest, @RequestParam String filter) {
        if (httpRequest.getSession().getAttribute("currentUser") != null) {
            User user = (User) httpRequest.getSession().getAttribute("currentUser");
            user = userDAO.findByUsername(user.getUsername()).get();
            timeFilter.addFilterForUser(user.getId(), filter);
        }
        return "redirect:/settings?show=filters";

    }

    @RequestMapping(value = "/add/group", method = RequestMethod.POST)
    public String addTimeFilterGroup(HttpServletRequest httpRequest, @RequestParam String groupName, @RequestParam String filter) {
        if (httpRequest.getSession().getAttribute("currentUser") != null) {
            Optional<StalkGroup> group = stalkGroupDAO.findByName(groupName);
            if (group.isPresent()) {
                timeFilter.addFilterForGroup(group.get().getId(), filter);
            }
        }
        return "redirect:/settings?show=filters";

    }

    @RequestMapping(value = "/removeAll", method = RequestMethod.GET)
    public String removeAll(HttpServletRequest httpRequest) {
        if (httpRequest.getSession().getAttribute("currentUser") != null) {
            User user = (User) httpRequest.getSession().getAttribute("currentUser");
            //            timeFilter.removeAllForUser(user.getUsername());
        }
        return "redirect:/settings?show=filters";

    }

}
