/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hlinka.model.StalkAccount;
import com.hlinka.model.User;
import com.hlinka.model.dao.StalkAccountDAO;
import com.hlinka.model.dao.UserDAO;
import com.hlinka.services.StalkAccountService;

/**
 * {description}
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Component
public class StalkAccountsServiceImpl
    implements StalkAccountService
{

    //TODO - add logging if there's time for it, remove exceptions

    private final StalkAccountDAO stalkAccountDAO;

    private final UserDAO userDAO;

    @Autowired
    public StalkAccountsServiceImpl(final StalkAccountDAO stalkAccountDAO, final UserDAO userDAO) {
        this.stalkAccountDAO = stalkAccountDAO;
        this.userDAO = userDAO;

    }

    @Override
    public void add(StalkAccount stalkAccount) {
        if (stalkAccountDAO.save(stalkAccount) == null) {
            throw new RuntimeException("Failed to add new account to stalk !");
        }
    }

    @Override
    public void remove(StalkAccount stalkAccount) {
        Optional<StalkAccount> accountFromDb = stalkAccountDAO.findByName(stalkAccount.getTitle());
        if (accountFromDb.isPresent()) {
            stalkAccountDAO.deleteById(accountFromDb.get().getId());
        } else {
            throw new RuntimeException("Failed to delete stalk account !");
        }
    }

    @Override
    public void modify(StalkAccount newAccount) {
    }

    @Override
    public void addForUser(StalkAccount stalkAccount, User user) {
        Optional<User> userFromDb = userDAO.findById(user.getId());
        if (userFromDb.isPresent()) {
            user = userFromDb.get();
            if (user.getStalkAccountList() == null) {
                user.setStalkAccountList(new ArrayList<>());
            }
            user.getStalkAccountList().add(stalkAccount);
            stalkAccountDAO.save(stalkAccount);

        }
    }

    @Override
    public void addForUserWithId(String name, Long userId) {
        Optional<User> userFromDb = userDAO.findById(userId);
        if (userFromDb.isPresent() && (userFromDb.get().getStalkAccountList() == null || !userFromDb.get().getStalkAccountList().stream()
                                                                                                    .filter(stalkAccount -> stalkAccount.getTitle().equalsIgnoreCase(name)).findFirst().isPresent()))
        {
            stalkAccountDAO.save(new StalkAccount(userId, name));
        }
    }

    @Override
    public void removeForUser(String name, String username) {
        Optional<User> userFromDb = userDAO.findByUsername(username);
        if (userFromDb.isPresent()) {
            findAndRemoveStalkAccount(userFromDb.get(), name);
        }
    }

    @Override
    public void removeAllForUser(String username) {
        Optional<User> userFromDb = userDAO.findByUsername(username);
        if (userFromDb.isPresent()) {
            userFromDb.get().getStalkAccountList().stream().forEach(stalkAccount -> stalkAccountDAO.deleteById(stalkAccount.getId()));
        }
    }

    private void findAndRemoveStalkAccount(User userFromDb, String name) {

        List<StalkAccount> stalkAccountList = userFromDb.getStalkAccountList().stream().filter(stalkAccount -> stalkAccount.getTitle().equalsIgnoreCase(name)).collect(Collectors.toList());
        if (stalkAccountList != null) {
            stalkAccountList.forEach(stalkAccount -> stalkAccountDAO.deleteById(stalkAccount.getId()));
        }

    }
}
