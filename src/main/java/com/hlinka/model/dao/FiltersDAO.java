/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.model.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hlinka.model.TimeFilter;

/**
 * {description}
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Repository
public class FiltersDAO
    extends DAO
{

    private static final String FIND_BY_ID = "SELECT * from filters f WHERE f.id = ?";
    private static final String FIND_BY_VALUE = "SELECT * from filters f WHERE f.valueInDays = ?";
    private static final String DELETE_BY_ID = "DELETE from filters WHERE id = ?";
    private static final String INSERT = "INSERT INTO filters (valueInDays) VALUES (?)";

    private static final String INSERT_GROUP_MAPPING = "INSERT INTO group_filters_mapping (groupId, filterId) VALUES (?, ?)";
    private static final String FIND_BY_GROUP_ID = "SELECT * FROM group_filters_mapping WHERE groupId = ?";
    private static final String DELETE_GROUP_MAPPING = "DELETE from group_filter_mapping WHERE groupId = ? AND filterId = ?";

    private static final String INSERT_ACCOUNT_MAPPING = "INSERT INTO account_filters_mapping (accountId, filterId) VALUES (?, ?)";
    private static final String FIND_BY_ACCOUNT_ID = "SELECT id FROM account_filters_mapping WHERE accountId = ?";
    private static final String DELETE_ACCOUNT_MAPPING = "DELETE from account_filters_mapping WHERE accountId = ? AND filterId = ?";

    private static final RowMapper<TimeFilter> TIME_FILTER_ROW_MAPPER = (rs, rowNum) -> {
        final TimeFilter timeFilter = new TimeFilter();
        timeFilter.setId(rs.getLong("id"));
        timeFilter.setValueInDays(rs.getLong("valueInDays"));

        return timeFilter;
    };

    @Transactional
    public List<TimeFilter> findMultipleByGroupId(final Long groupId) {
        List<Long> listOfFilterIdsToAdd = jdbcTemplate.queryForList(FIND_BY_GROUP_ID, new Object[] { groupId }, Long.class);
        List<TimeFilter> timeFilterList = new ArrayList<>();
        listOfFilterIdsToAdd.forEach(id -> {
            Optional<TimeFilter> timeFilter = findById(id);
            if (timeFilter.isPresent()) {
                timeFilterList.add(timeFilter.get());
            }
        });

        return timeFilterList;
    }

    @Transactional
    public List<TimeFilter> findMultipleByAccountId(final Long accountId) {
        List<Long> listOfFilterIdsToAdd = jdbcTemplate.queryForList(FIND_BY_ACCOUNT_ID, new Object[] { accountId }, Long.class);
        List<TimeFilter> timeFilterList = new ArrayList<>();
        listOfFilterIdsToAdd.forEach(id -> {
            Optional<TimeFilter> timeFilter = findById(id);
            if (timeFilter.isPresent()) {
                timeFilterList.add(timeFilter.get());
            }
        });

        return timeFilterList;
    }

    @Transactional
    public Optional<TimeFilter> findById(final Long id) {
        Optional<TimeFilter> timeFilter = Optional.empty();
        try {
            timeFilter = Optional.of(jdbcTemplate.queryForObject(FIND_BY_ID, new Object[] { id }, TIME_FILTER_ROW_MAPPER));
        } catch (EmptyResultDataAccessException e) {
            System.out.println(e);
        }

        return timeFilter;
    }

    @Transactional
    public Long deleteById(final Long id) {

        int retVal = jdbcTemplate.update(DELETE_BY_ID, new Object[] { id });

        return Long.valueOf(retVal);
    }

    @Transactional
    public void deleteForGroup(final Long groupId, final Long filterId) {
        jdbcTemplate.update(DELETE_GROUP_MAPPING, new Object[] { groupId, filterId });
    }

    @Transactional
    public void deleteForAccount(final Long accountId, final Long filterId) {
        jdbcTemplate.update(DELETE_ACCOUNT_MAPPING, new Object[] { accountId, filterId });
        deleteById(filterId);
    }

    @Transactional
    public Optional<TimeFilter> findByValue(final Long value) {
        Optional<TimeFilter> timeFilter = Optional.empty();
        try {
            timeFilter = Optional.of(jdbcTemplate.queryForObject(FIND_BY_VALUE, new Object[] { value }, TIME_FILTER_ROW_MAPPER));
        } catch (EmptyResultDataAccessException e) {

        }
        return timeFilter;
    }

    @Transactional
    public void save(final TimeFilter timeFilter) {

        jdbcTemplate.update(INSERT, new Object[] {
            timeFilter.getValueInDays()
        });
    }

    @Transactional
    public void addFilterForGroup(Long groupId, String filter) {
        try {
            //TODO - optimize filter saving - many of one kind
            save(new TimeFilter(Long.valueOf(filter)));
            jdbcTemplate.update(INSERT_GROUP_MAPPING, new Object[] { groupId, getLastInsertId() });
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Transactional
    public void addFilterForUser(Long userId, String filter) {
        try {
            //TODO - optimize filter saving - many of one kind
            save(new TimeFilter(Long.valueOf(filter)));
            jdbcTemplate.update(INSERT_ACCOUNT_MAPPING, new Object[] { userId, getLastInsertId() });
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
