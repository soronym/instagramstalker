/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.model.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hlinka.model.StalkAccount;

/**
 * {description}
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Repository
public class StalkAccountDAO
    extends DAO
{

    private static final String FIND_BY_ID = "SELECT * from stalk_account sa WHERE sa.id = ?";
    private static final String FIND_BY_NAME = "SELECT * from stalk_account sa WHERE sa.title LIKE ?";
    private static final String FIND_BY_USERNAME = "SELECT * from stalk_account sa JOIN user u ON u.id = sa.userId WHERE u.username LIKE ?";
    private static final String FIND_BY_GROUP_ID = "SELECT * from stalk_account sa WHERE sa.groupId = ?";
    private static final String INSERT_STALK_ACCOUNT = "INSERT INTO stalk_account (userId, title) VALUES (?, ?)";
    private static final String DELETE_BY_ID = "DELETE from stalk_account WHERE id = ?";

    private static final BeanPropertyRowMapper<StalkAccount> MAPPER = new BeanPropertyRowMapper<>(StalkAccount.class);

    private static final RowMapper<StalkAccount> STALK_ACCOUNT_ROW_MAPPER = (rs, rowNum) -> {
        final StalkAccount stalkAccount = new StalkAccount();
        stalkAccount.setId(rs.getLong("id"));
        stalkAccount.setUserId(rs.getLong("userId"));
        stalkAccount.setTitle(rs.getString("title"));
        return stalkAccount;
    };

    @Transactional
    public List<StalkAccount> findMultipleByUsernameWithoutDuplicities(final String username) {
        List<StalkAccount> stalkAccountList = jdbcTemplate.query(FIND_BY_USERNAME, new Object[] { username }, STALK_ACCOUNT_ROW_MAPPER);

        return removeDuplicities(stalkAccountList);
    }

    private List<StalkAccount> removeDuplicities(List<StalkAccount> stalkAccountList) {
        List<StalkAccount> retVal = new ArrayList<>();
        for (StalkAccount stalkAccount : stalkAccountList) {
            boolean inList = false;
            for (StalkAccount iterator : retVal) {
                if (iterator.getTitle().equals(stalkAccount.getTitle())) {
                    inList = true;
                    break;
                }
            }
            if (!inList) {
                retVal.add(stalkAccount);
            }
        }
        return retVal;
    }

    @Transactional
    public List<StalkAccount> findByGroupId(final Long groupId) {
        List<StalkAccount> stalkAccountList = jdbcTemplate.query(FIND_BY_GROUP_ID, new Object[] { groupId }, new BeanPropertyRowMapper(StalkAccount.class));

        return stalkAccountList;
    }

    @Transactional
    public Optional<StalkAccount> findById(final Long id) {
        Optional<StalkAccount> stalkAccount = Optional.empty();
        try {
            stalkAccount = Optional.of(jdbcTemplate.queryForObject(FIND_BY_ID, new Object[] { id }, MAPPER));
        } catch (EmptyResultDataAccessException e) {

        }

        return stalkAccount;
    }

    @Transactional
    public Long deleteById(final Long id) {

        int retVal = jdbcTemplate.update(DELETE_BY_ID, new Object[] { id });

        return Long.valueOf(retVal);
    }

    @Transactional
    public Optional<StalkAccount> findByName(final String name) {
        Optional<StalkAccount> stalkAccount = Optional.empty();
        try {
            stalkAccount = Optional.of(jdbcTemplate.queryForObject(FIND_BY_NAME, new Object[] { name }, MAPPER));
        } catch (EmptyResultDataAccessException e) {

        }
        return stalkAccount;
    }

    @Transactional
    public Long save(final StalkAccount stalkAccount) {

        int retVal = jdbcTemplate.update(INSERT_STALK_ACCOUNT, new Object[] {
            stalkAccount.getUserId(), stalkAccount.getTitle()
        });

        return Long.parseLong(String.valueOf(retVal));
    }
}
