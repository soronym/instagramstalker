/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.model.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hlinka.model.StalkAccount;
import com.hlinka.model.StalkGroup;

/**
 * {description}
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Repository
public class StalkGroupDAO
    extends DAO
{

    private static final String FIND_BY_ID = "SELECT * from stalk_group sg WHERE sg.id = ?";
    private static final String FIND_BY_NAME = "SELECT * from stalk_group WHERE title LIKE ?";
    private static final String FIND_BY_USERNAME = "SELECT * from stalk_group sg JOIN user u ON sg.userId=u.id WHERE u.username LIKE ?";
    private static final String INSERT_STALK_GROUP = "INSERT INTO stalk_group (title, userId) VALUES (?, ?)";
    private static final String FIND_STALK_ACCOUNTS_FOR_GROUP = "SELECT stalkAccountId FROM group_account_mapping gaid WHERE gaid.groupId = ?";
    private static final String FIND_MAPPINGS_FOR_GROUP = "SELECT id FROM group_account_mapping gaid WHERE gaid.groupId = ?";
    private static final String SAVE_MAPPING = "INSERT INTO group_account_mapping (groupId, stalkAccountId) VALUES (?, ?)";
    private static final String DELETE_BY_ID = "DELETE FROM stalk_group WHERE id = ?";
    private static final String DELETE_STALK_MAPPING_BY_ID = "DELETE FROM group_account_mapping WHERE id = ?";

    private static final RowMapper<StalkGroup> STALK_GROUP_ROW_MAPPER = (rs, rowNum) -> {
        final StalkGroup stalkGroup = new StalkGroup();
        stalkGroup.setId(rs.getLong("id"));
        stalkGroup.setUserId(rs.getLong("userId"));
        stalkGroup.setTitle(rs.getString("title"));
        return stalkGroup;
    };

    private final JdbcTemplate jdbcTemplate;
    private final StalkAccountDAO stalkAccountDAO;
    private final FiltersDAO filtersDAO;

    @Autowired
    public StalkGroupDAO(final JdbcTemplate jdbcTemplate, final StalkAccountDAO stalkAccountDAO, final FiltersDAO filtersDAO) {
        this.jdbcTemplate = jdbcTemplate;
        this.stalkAccountDAO = stalkAccountDAO;
        this.filtersDAO = filtersDAO;
    }

    private void mapFiltersToGroups(List<StalkGroup> retVal) {
        if (retVal != null) {
            retVal.forEach(stalkGroup -> stalkGroup.setFilters(filtersDAO.findMultipleByGroupId(stalkGroup.getId())));
        }
    }

    @Transactional
    public List<StalkGroup> findMultipleByUsername(final String username) {
        List<StalkGroup> retVal = jdbcTemplate.query(FIND_BY_USERNAME, new Object[] {
            username
        }, STALK_GROUP_ROW_MAPPER);
        mapStalkAccountsToGroups(retVal);
        mapFiltersToGroups(retVal);
        return retVal;
    }

    @Transactional
    public Long deleteById(final Long id) {

        int retVal = jdbcTemplate.update(DELETE_BY_ID, new Object[] { id });

        return Long.valueOf(retVal);
    }

    @Transactional
    private Long deleteMappingById(final Long id) {

        int retVal = jdbcTemplate.update(DELETE_STALK_MAPPING_BY_ID, new Object[] { id });

        return Long.valueOf(retVal);
    }

    @Transactional
    public Optional<StalkGroup> findById(final Long id) {
        Optional<StalkGroup> stalkGroup = Optional.empty();
        try {
            stalkGroup = Optional.of(jdbcTemplate.queryForObject(FIND_BY_ID, new Object[] { id }, STALK_GROUP_ROW_MAPPER));
        } catch (EmptyResultDataAccessException e) {

        }
        if (stalkGroup.isPresent()) {
            mapStalkAccountsToGroup(stalkGroup.get());
        }

        return stalkGroup;
    }

    @Transactional
    public Optional<StalkGroup> findByName(final String name) {
        Optional<StalkGroup> stalkGroup = Optional.empty();
        try {
            stalkGroup = Optional.of(jdbcTemplate.queryForObject(FIND_BY_NAME, new Object[] { name }, STALK_GROUP_ROW_MAPPER));
        } catch (EmptyResultDataAccessException e) {

        }
        if (stalkGroup.isPresent()) {
            mapStalkAccountsToGroup(stalkGroup.get());
        }

        return stalkGroup;
    }

    private void mapStalkAccountsToGroup(StalkGroup stalkGroup) {

        List<Long> longList = jdbcTemplate.queryForList(FIND_STALK_ACCOUNTS_FOR_GROUP, new Object[] { stalkGroup.getId() }, Long.class);
        List<StalkAccount> stalkAccountList = new ArrayList<>();
        for (Long id : longList) {
            Optional<StalkAccount> stalkAccount = stalkAccountDAO.findById(id);
            if (stalkAccount.isPresent()) {
                stalkAccountList.add(stalkAccount.get());
            }
        }
        stalkGroup.setStalkAccountList(stalkAccountList);
    }

    private void mapStalkAccountsToGroups(List<StalkGroup> retVal) {
        for (StalkGroup stalkGroup : retVal) {
            List<Long> longList = jdbcTemplate.queryForList(FIND_STALK_ACCOUNTS_FOR_GROUP, new Object[] { stalkGroup.getId() }, Long.class);
            List<StalkAccount> stalkAccountList = new ArrayList<>();
            for (Long id : longList) {
                Optional<StalkAccount> stalkAccount = stalkAccountDAO.findById(id);
                if (stalkAccount.isPresent()) {
                    stalkAccountList.add(stalkAccount.get());
                }
            }
            stalkGroup.setStalkAccountList(stalkAccountList);
        }
        removeEmptyGroups(retVal);
    }

    private void removeEmptyGroups(List<StalkGroup> retVal) {
        List<StalkGroup> groupsToRemove = new ArrayList<>();
        retVal.forEach(stalkGroup -> {
            if (stalkGroup.getStalkAccountList() == null || stalkGroup.getStalkAccountList().size() == 0) {
                List<Long> longList = jdbcTemplate.queryForList(FIND_MAPPINGS_FOR_GROUP, new Object[] { stalkGroup.getId() }, Long.class);

                if (longList != null) {
                    longList.forEach(id -> deleteMappingById(id));
                }
                deleteById(stalkGroup.getId());
                groupsToRemove.add(stalkGroup);
            }
        });
        retVal.removeAll(groupsToRemove);
    }

    @Transactional
    public Long save(final StalkGroup stalkGroup) {

        jdbcTemplate.update(INSERT_STALK_GROUP, new Object[] {
            stalkGroup.getTitle(), stalkGroup.getUserId()
        });
        int stalkGroupInt = getLastInsertId();
        if (stalkGroup.getStalkAccountList() != null) {

            stalkGroup.getStalkAccountList().forEach(stalkAccount -> {
                stalkAccountDAO.save(stalkAccount);
                jdbcTemplate.update(SAVE_MAPPING, new Object[] {
                    stalkGroupInt, getLastInsertId()
                });
            });

        }

        return Long.valueOf(stalkGroupInt);
    }

}
