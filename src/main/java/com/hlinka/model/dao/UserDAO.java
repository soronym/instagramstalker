/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.model.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hlinka.model.User;

/**
 * Data access object for {@link User}.
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
@Repository
public class UserDAO {

    private static final String FIND_BY_ID = "SELECT * FROM user u WHERE u.id = ?";
    private static final String FIND_BY_USERNAME = "SELECT * FROM user u WHERE u.username LIKE ?";
    private static final String INSERT_USER = "INSERT INTO user (username, password, email) VALUES (?, ?, ?)";
    //    private static final BeanPropertyRowMapper<User> MAPPER = new BeanPropertyRowMapper<>(User.class);

    private static final RowMapper<User> MAPPER = (rs, rowNum) -> {
        final User user = new User();
        user.setId(rs.getLong("id"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        user.setEmail(rs.getString("email"));
        return user;
    };

    private final JdbcTemplate jdbcTemplate;
    private final StalkGroupDAO stalkGroupDAO;
    private final StalkAccountDAO stalkAccountDAO;
    private final FiltersDAO filtersDAO;

    @Autowired
    public UserDAO(final JdbcTemplate jdbcTemplate, final StalkGroupDAO stalkGroupDAO, final StalkAccountDAO stalkAccountDAO, final FiltersDAO filtersDAO) {
        this.jdbcTemplate = jdbcTemplate;
        this.stalkGroupDAO = stalkGroupDAO;
        this.stalkAccountDAO = stalkAccountDAO;
        this.filtersDAO = filtersDAO;
    }

    @Transactional
    public Optional<User> findById(final Long id) {
        Optional<User> user = Optional.empty();
        try {
            user = Optional.of(jdbcTemplate.queryForObject(FIND_BY_ID, new Object[] { id }, MAPPER));
            addListsForUser(user.get());
        } catch (EmptyResultDataAccessException e) {

        }
        return user;
    }

    private void addListsForUser(User user) {
        mapStalkAccounts(user);
        mapStalkGroups(user);
        mapTimeFilters(user);
    }

    private void mapTimeFilters(User user) {
        user.setTimeFilterList(filtersDAO.findMultipleByAccountId(user.getId()));
    }

    private void mapStalkGroups(User user) {
        user.setStalkGroupList(stalkGroupDAO.findMultipleByUsername(user.getUsername()));
    }

    private void mapStalkAccounts(User user) {
        user.setStalkAccountList(stalkAccountDAO.findMultipleByUsernameWithoutDuplicities(user.getUsername()));
    }

    @Transactional
    public Optional<User> findByUsername(final String username) {
        Optional<User> user = Optional.empty();
        try {
            user = Optional.of(jdbcTemplate.queryForObject(FIND_BY_USERNAME, new Object[] { username }, MAPPER));
            addListsForUser(user.get());
        } catch (EmptyResultDataAccessException e) {

        }

        return user;
    }

    @Transactional
    public Long save(final User user) {

        int retVal = jdbcTemplate.update(INSERT_USER, new Object[] {
            user.getUsername(), user.getPassword(), user.getEmail()
        });

        if (user.getStalkAccountList() != null) {
            user.getStalkAccountList().forEach(stalkAccount -> stalkAccountDAO.save(stalkAccount));
        }
        if (user.getStalkGroupList() != null) {
            user.getStalkGroupList().forEach(stalkGroup -> stalkGroupDAO.save(stalkGroup));
        }

        return Long.parseLong(String.valueOf(retVal));
    }

}