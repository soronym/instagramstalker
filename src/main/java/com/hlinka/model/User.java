/*
 * Copyright (C) 2016 Norvax, Inc.
 * All Rights Reserved
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Norvax, Inc.; the contents
 * of this file may not be disclosed to third parties, copied or duplicated
 * in any form, in whole or in part, without the prior written permission of
 * Norvax, Inc. The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 *
 * Permission is hereby granted solely to the licensee for use of this source
 * code in its unaltered state. This source code may not be modified by
 * licensee except under direction of Norvax, Inc. Neither may this source
 * code be given under any circumstances to non-licensees in any form,
 * including source or binary. Modification of this source constitutes breach
 * of contract, which voids any potential pending support responsibilities by
 * Norvax, Inc. Divulging the exact or paraphrased contents of this source
 * code to unlicensed parties either directly or indirectly constitutes
 * violation of federal and international copyright and trade secret laws, and
 * will be duly prosecuted to the fullest extent permitted under law.
 *
 * This software is provided by Norvax, Inc. ``as is'' and any express or
 * implied warranties, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose are disclaimed. In
 * no event shall the regents or contributors be liable for any direct,
 * indirect, incidental, special, exemplary, or consequential damages
 * (including, but not limited to, procurement of substitute goods or
 * services; loss of use, data, or profits; or business interruption) however
 * caused and on any theory of liability, whether in contract, strict
 * liability, or tort (including negligence or otherwise) arising in any way
 * out of the use of this software, even if advised of the possibility of such
 * damage.
 */

package com.hlinka.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.hlinka.model.wrappers.UserRegistrationWrapper;

/**
 * Object backing user.
 *
 * @author <a href="mailto:ivan.hlinka@thinkcreatix.com">Ivan Hlinka</a>
 */
public class User {

    private Long id;

    @NotNull
    @NotEmpty
    @Size(min = 3)
    private String username;

    @NotNull
    @NotEmpty
    @Size(min = 8)
    private String password;

    private String email;

    private List<StalkAccount> stalkAccountList;

    private List<StalkGroup> stalkGroupList;

    private List<TimeFilter> timeFilterList;

    /**
     * Default constructor.
     */
    public User() {
    }

    /**
     * Constructor using {@link UserRegistrationWrapper}.
     *
     * @param userRegistrationWrapper the wrapped from registration jsp
     */
    public User(UserRegistrationWrapper userRegistrationWrapper) {
        this.username = userRegistrationWrapper.getUsername();
        this.password = userRegistrationWrapper.getPassword();
        this.email = userRegistrationWrapper.getEmail();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<StalkAccount> getStalkAccountList() {
        return stalkAccountList;
    }

    public void setStalkAccountList(List<StalkAccount> stalkAccountList) {
        this.stalkAccountList = stalkAccountList;
    }

    public List<StalkGroup> getStalkGroupList() {
        return stalkGroupList;
    }

    public void setStalkGroupList(List<StalkGroup> stalkGroupList) {
        this.stalkGroupList = stalkGroupList;
    }

    public List<TimeFilter> getTimeFilterList() {
        return timeFilterList;
    }

    public void setTimeFilterList(List<TimeFilter> timeFilterList) {
        this.timeFilterList = timeFilterList;
    }
}
