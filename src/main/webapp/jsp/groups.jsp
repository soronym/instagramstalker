<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="main" class="container col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-top min-height-80 px-14">
    <div id="addAccount">
        <form:form name="newStalkGroupForm" method="post" action="/stalkGroup/add">
            <div class="col-lg-12 text-center">
                <label class="h1">Add group</label>
            </div>
            <div class="col-lg-12">
                <div class="col-lg-4">
                    <div class="col-lg-5 text-left">
                        <label>Group name</label>
                    </div>
                    <div class="col-lg-7 input-block-level">
                        <input name="newStalkGroup" type="text"/>
                    </div>
                </div>
                <div class="col-lg-7 text-left">
                    <div class="col-lg-2">
                        <label>Accounts</label>
                    </div>
                    <div class="col-lg-10 input-block-level">
                        <input class="input-group" name="newStalkGroupList" type="text"/>
                    </div>
                </div>
                <div class="col-lg-1">
                    <button type="submit" class="btn btn-send">Add</button>
                </div>
            </div>
        </form:form>
    </div>
    <div id="stalkList" class="col-lg-12 padding-top-10 text-center">
        <c:forEach items="${stalkGroupList}" var="item" varStatus="varStatus">
            <div class="">
                <label>
                        ${item.title}
                </label>
                <span class="padding-left-10 btn">
                        <a type="submit" class="btn btn-danger" href="/stalkGroup/remove?name=${item.title}">Remove</a>
                </span>
            </div>
        </c:forEach>
    </div>
    <div class="text-right col-lg-12 padding-top-10 padding-top-10">
        <a type="submit" class="btn btn-send" href="/stalkGroup/removeAll">Remove All</a>
    </div>
</div>
