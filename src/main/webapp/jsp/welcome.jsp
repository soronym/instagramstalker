<!DOCTYPE html>
<html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="UTF-8"/>
    <title>Welcome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="../css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
<section class="container padding-top height-100">
    <div class="row col-lg-2 text-left">
        <div class="logo">
            <div class="logo-card">
                <img src="../insta-logo.png"/>
            </div>
        </div>
    </div>
    <div class="col-lg-10">
        <div class="row text-right">
            <a href="/login" class="padding-bottom-10"><spring:message code="login"></spring:message></a>
        </div>
        <div class="row text-right">
            <a href="/registration" class="padding-bottom-10"><spring:message code="registration"></spring:message></a>
        </div>
    </div>
    <div class="col-lg-12 padding-top-20 height-80">
        <span class="col-lg-12 about-product"><spring:message code="about.product.1"/></span>
        <span class="col-lg-12 about-product padding-left-20"><spring:message code="about.product.2"/></span>
        <span class="col-lg-12 about-product padding-left-30"><spring:message code="about.product.3"/></span>
    </div>
    </div>
    <%--<div class="col-lg-12 author text-right bottom-right">--%>
        <%--<spring:message code="about.autor"/>--%>
    <%--</div>--%>
    <div class="col-lg-12 author"><spring:message code="disclaimer"/></div>
</section>
</body>
</html>