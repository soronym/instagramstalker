<!DOCTYPE html>
<html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="UTF-8"/>
    <title>Settings</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="../css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
<section class="col-offset-2 col-8 col-offset-2 padding-top">

    <%--TODO - add logout--%>

    <div id="left-bar" class="col-lg-2 text-center">

        <div class="logo" style="background-color: white">
            <div class="logo-card">
                <img src="../insta-logo.png"/>
            </div>
        </div>

        <div class="padding-top-50 padding-bottom-10">
            <span class="" style="font-size: 120%;">
                <spring:message code="settings.for"/>
            </span>
        </div>

        <div class="text-left  padding-bottom-10">
            <a id="groupsBtn" href="/settings?show=groups">
                <spring:message code="group.settings"/>
            </a>
        </div>

        <div class="text-left  padding-bottom-10">
            <a id="filtersBtn" href="/settings?show=filters">
                <spring:message code="filter.settings"/>
            </a>
        </div>

        <div class="text-left  padding-bottom-10">
            <a id="accountsBtn" href="/settings?show=accounts">
                <spring:message code="accounts.settings"/>
            </a>
        </div>

        <div class="text-left  padding-bottom-10">
            <a id="notificationsBtn" href="/settings?show=notifications">
                <spring:message code="notifications.settings"/>
            </a>
        </div>

        <div class="text-left  padding-bottom-10">
            <a class="btn btn-send" href="/main"><spring:message code="back.to.mainPage"/></a>
        </div>
    </div>
    <div id="main" class="col-lg-10 padding-top min-height-80" style="background-color: white;">
        <div class="text-left  text-center" style="font-size: 50px">
            <c:if test="${groups}">
                <div id="groups" class="settings">
                    <jsp:include page="groups.jsp"/>
                </div>
            </c:if>

            <c:if test="${accounts}">
                <div id="accounts" class="settings">
                    <jsp:include page="accounts.jsp"/>
                </div>
            </c:if>

            <c:if test="${notifications}">
                <div id="notifications" class="settings">
                    <jsp:include page="notifications.jsp"/>
                </div>
            </c:if>

            <c:if test="${filters}">
                <div id="filters" class="settings">
                    <jsp:include page="filters.jsp"/>
                </div>
            </c:if>
        </div>
    </div>
</section>
</body>
</html>