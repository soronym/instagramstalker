<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="main" class="container col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-top min-height-80 px-14">
    <div id="addAccount">
        <form:form name="addStalkAccountForm" method="post" action="/stalkAccount/add">
            <div class="col-lg-2 text-left">
                <label>Add account:</label>
            </div>
            <div class="col-lg-3">
                <input name="newStalkAccount" type="text"/>
            </div>
            <div class="padding-left-20 col-lg-1">
                <button type="submit" class="btn btn-send">Add</button>
            </div>
            <div class="col-lg-6"/>
        </form:form>
    </div>
    <div id="stalkList" class="col-lg-12 padding-top-5 text-center">
        <div class="h1">Account list</div>
        <c:forEach items="${stalkAccountList}" var="item" varStatus="varStatus">
            <div class="col-lg-8 text-right">
                <label>
                        ${item.title}
                </label>
            </div>
            <div class="col-lg-4 padding-bottom-1">
                <span class="btn text-left">
                    <a type="submit" class="btn btn-danger" href="/stalkAccount/remove?name=${item.title}">Remove</a>
                </span>
            </div>
        </c:forEach>
    </div>
    <div class="text-right col-lg-12 padding-top-10 padding-top-10">
        <a type="submit" class="btn btn-send" href="/stalkAccount/removeAll">Remove All</a>
    </div>
</div>
