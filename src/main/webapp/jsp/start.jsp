<!DOCTYPE html>
<html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="UTF-8"/>
    <title>Log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="../css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
<form:form action="/start" method="post">
    <section class="font-style-liberation">

        <section class="container  col-lg-6 col-lg-offset-3 col-xs-12">
            <div class="form-group bkgr-white center-block">
                <div class="row col-12 text-center v-margin-1">
                    <div><spring:message code="label.password"/></div>
                    <input type="password" name="password" placeholder="Input master password"/>
                </div>
                <section class="row text-center">
                    <button type="submit" class="btn btn-send ">Confirm</button>
                </section>

            </div>
        </section>
    </section>
</form:form>
</body>
</html>