<!DOCTYPE html>
<html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="UTF-8"/>
    <title>Log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="../css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
<form:form action="/login" modelAttribute="user" method="post">
    <section class="font-style-liberation">

        <section class="container  col-lg-6 col-lg-offset-3 col-xs-12">
            <div class="card card-container">
                <img class="logo-card" src="../insta-logo.png"/>
                <div class="title-card form-group bkgr-color-title text-center">
                    <div>
                        <h1>Login</h1>
                    </div>
                </div>
                <div class="form-group bkgr-color-form center-block">
                    <div class="row col-12 text-center v-margin-1">
                        <form:label path="username">
                            <div><spring:message code="label.username"/></div>
                            <form:input type="text" path="username"/>
                            <form:errors cssClass="row text-center" path="username"/>
                        </form:label>
                    </div>
                </div>

                <div class="form-group bkgr-white center-block">
                    <div class="row col-12 text-center v-margin-1">
                        <form:label path="password">
                            <div><spring:message code="label.password"/></div>
                            <form:input type="password" path="password"/>
                            <form:errors cssClass="center-block" path="password"/>
                        </form:label>
                    </div>
                    <section class="row text-center col-xs-offset-8 col-sm-offset-8 col-md-offset-8 col-lg-offset-8 col-4">
                        <button type="submit" class="btn btn-send ">Confirm</button>
                    </section>

                    <section class="row text-center register-now">
                        <div class="row">
                            <label><spring:message code="register.now"/> </label>
                        </div>
                        <div class="row">
                            <a href="/registration" class="btn btn-send">Register</a>
                        </div>
                        <div>
                            ${loginError}
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</form:form>
</body>
</html>