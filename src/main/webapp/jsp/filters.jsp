<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="main" class="container col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-top min-height-80 px-14">
    <div id="addFilterForAccount" class="col-lg-12">
        <div class="h2 col-lg-12">
            Add time filter for account
        </div>
        <div class="col-lg-12">
            <form:form name="addTimeFilterForm" method="post" action="/timeFilter/add/account">
                <div class="col-lg-10">
                    <div class="text-right col-lg-6">
                        <label>Show posts newer than input number of days: </label>
                    </div>
                    <div class="col-lg-1">
                            <%--TODO - add dropdown--%>
                        <input name="filter" type="text"/>
                    </div>
                    <div class="col-lg-5"></div>
                </div>
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-send">Add</button>
                </div>
            </form:form>
        </div>
    </div>

    <%--<div id="addFilter" class="padding-top-5 col-lg-12">--%>
        <%--<div class="h2 col-lg-12">--%>
            <%--Add time filter for group--%>
        <%--</div>--%>
        <%--<div class="col-lg-12">--%>
            <%--<form:form name="addTimeFilterForm" method="post" action="/timeFilter/add/group">--%>
                <%--<div class="col-lg-10">--%>
                    <%--<div class="col-lg-8  text-right">--%>
                        <%--<div class="col-lg-10">--%>
                            <%--<label>Show posts newer than input number of hours: </label>--%>
                        <%--</div>--%>
                        <%--<div class="col-lg-2">--%>
                                <%--&lt;%&ndash;TODO - add dropdown&ndash;%&gt;--%>
                            <%--<input name="filter" type="text"/>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                    <%--<div class="col-lg-4">--%>
                        <%--<div class="col-lg-7">--%>
                            <%--<label>group name: </label>--%>
                        <%--</div>--%>
                        <%--<div class="col-lg-5">--%>
                                <%--&lt;%&ndash;TODO - add dropdown&ndash;%&gt;--%>
                            <%--<input name="groupName" type="text"/>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
                <%--<div class="col-lg-2">--%>
                    <%--<button type="submit" class="btn btn-send">Add</button>--%>
                <%--</div>--%>

            <%--</form:form>--%>
        <%--</div>--%>
    <%--</div>--%>

    <%--<div id="stalkList" class="col-lg-12 padding-top-5 text-center">--%>
        <%--<div class="h2">Time filters for groups</div>--%>
        <%--<c:forEach items="${stalkGroupList}" var="item" varStatus="varStatus">--%>
            <%--<div class="col-lg-8 text-right">--%>
                <%--<label>--%>
                        <%--${item.title}--%>
                <%--</label>--%>
            <%--</div>--%>
            <%--<div class="col-lg-4 padding-bottom-1">--%>
                <%--<c:forEach items="${item.filters}" var="filer" varStatus="filterStatus">--%>
                    <%--<div class="col-lg-7">--%>
                        <%--<label>${filter.valueInDays}</label>--%>
                    <%--</div>--%>
                    <%--<div class="col-lg-5">--%>
                        <%--<span class="btn text-left">--%>
                            <%--<a type="submit" class="btn btn-danger" href="/timeFilter/remove/group?groupId=${item.id}&filterId=${filter.id}">Remove</a>--%>
                        <%--</span>--%>
                    <%--</div>--%>
                <%--</c:forEach>--%>
            <%--</div>--%>
        <%--</c:forEach>--%>
    <%--</div>--%>

    <div class="col-lg-12 padding-top-5 text-center">
        <div class="h2 col-lg-12">Time filters for account</div>
        <div class="col-lg-12 text-left">
            <label>
                Time filter values in days:
            </label>
        </div>
        <c:forEach items="${timeFiltersUser}" var="item" varStatus="varStatus">
            <div class="col-lg-12">
                <div class="col-lg-1">
                    <label>
                            ${item.valueInDays}
                    </label>
                </div>
                <div class="col-lg-11 text-right">
            <span class="btn">
                    <a type="submit" class="btn btn-danger" href="/timeFilter/remove/account?filterId=${item.id}">Remove</a>
            </span>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
