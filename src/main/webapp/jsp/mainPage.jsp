<!DOCTYPE html>
<html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <meta charset="UTF-8"/>
    <title>Stalk</title>
    <link rel="stylesheet" href="webjars/bootstrap/3.3.4/css/bootstrap.min.css" type="text/css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../css/main.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1 padding-top">

    <%--TODO - add logout--%>

    <div id="left-bar" class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">

        <div class="logo" style="background-color: white">
            <div class="logo-card">
                <img src="../insta-logo.png"/>
            </div>
        </div>
        <div class="row search-bar padding-top-50">
            <div>
                <input type="text" placeholder="search for">
                <%--TODO - finish search--%>
            </div>
            <div>
                <button type="submit" class="btn btn-send"><spring:message code="button"/></button>
            </div>
        </div>
        <div id="groups" class="padding-groups">
            <c:forEach items="${stalkGroupList}" var="currentGroup" varStatus="currentGroupStatus">
                <div class="padding-bottom-5">
                    <a href="">${currentGroup.title}</a>
                </div>
            </c:forEach>
        </div>
        <div id="settings">
            <a href="/settings" class="btn btn-send">
                <spring:message code="settings"/>
            </a>
        </div>

    </div>
    <div id="main" class="col-xs-10 col-sm-10 col-md-10 col-lg-10 padding-top min-height-80 background-color">
        <%--<div class="text-right">--%>
        <%--<spring:message code="todo.notifications"/>--%>
        <%--</div>--%>

        <c:if test="${newLinksBool}">
            <c:forEach items="${links}" var="item" varStatus="itemStatus">
                <div class="text-center col-xs-10 col-sm-10 col-md-10 col-lg-10" style="font-size: 50px">
                    <div>
                        <a href="${item.link}">
                            <img src="${item.link}" class="photo"/>
                        </a>

                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <div class="text-left padding-top-100">
                            ${item.caption}
                    </div>
                </div>
            </c:forEach>
        </c:if>

        <c:if test="${oldLinksBool}">
            <div class="col-lg-12 text-center h2">
                Not meeting filter criteria
            </div>
            <c:forEach items="${oldLinks}" var="item" varStatus="itemStatus">
                <div class="text-center col-xs-10 col-sm-10 col-md-10 col-lg-10" style="font-size: 50px">
                    <div>
                        <a href="${item.link}">
                            <img src="${item.link}" class="photo"/>
                        </a>

                    </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <div class="text-left padding-top-100">
                            ${item.caption}
                    </div>
                </div>
            </c:forEach>
        </c:if>


    </div>
</div>
</body>
</html>